import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Injectable } from '@angular/core';
import { FormArray } from '@angular/forms/src/model';
import { FileModel } from '../models/file.model';

@Injectable()
export class FileService {  

  constructor(private http: Http) { }

  uploadFile(url: string, file: File) {
    let formData: FormData = new FormData();  
    formData.append('uploadFile', file, file.name);  
    let headers = new Headers();
    // headers.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({ headers: headers });  
    return this.http.post(url, formData, options)
                    .map((resp: Response) => resp)
                    .catch((error: any) => { return Observable.throw(error); });
  }

  sendNoProgressFilesIds(idsArray: FormArray, url: string) {
    const body = JSON.stringify(idsArray.getRawValue());
    let formArray: FormArray;
    // formArray.push('uploadFile', idsArray, file.name);  

    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.post(url, body, { headers: headers })
                    .map((resp: Response) => resp.json())
                    .catch((error: any) => { return Observable.throw(error); });
  }


  getNoProcessedFiles(url: string) {
    return this.http.get(url)
                    .map((resp: Response) => {
      let filesList = resp.json();
      let files: FileModel[] = [];
      for (let index in filesList) {
        console.log(filesList[index]);
        let file = filesList[index];
        console.log(file.FileName);
        files.push({ id: file.Id, fileName: file.FileName, fileStatus: 'no processed' });
      }
      return files;
    });
  }
}
