import { EventEmitter, Injectable } from "@angular/core";
import { UUID } from "angular2-uuid";
import { FileModel } from "../models/file.model";
declare var $: any;

@Injectable()
export class StatusFileService {
    private proxy: any;
    private proxyName: string = 'statusHub';
    private connection: any;  
    // create the Event Emitter  
    public statusReceived: EventEmitter<string>;  
    public dataReceived: EventEmitter<FileModel>;  
    public connectionEstablished: EventEmitter<Boolean>;  
    public connectionExists: Boolean;  
    constructor() {  
        // Constructor initialization  
        this.connectionEstablished = new EventEmitter<Boolean>();   
        this.dataReceived = new EventEmitter<FileModel>();  
        this.connectionExists = false;  
        // create hub connection  
        // this.connection = $.hubConnection('http://localhost:51018');
        this.connection = $.hubConnection('http://localhost:8080');        
        // create new proxy as name already given in top  
        this.proxy = this.connection.createHubProxy(this.proxyName);  
        // register on server events  
        this.registerOnServerEvents();  
        // call the connecion start method to start the connection to send and receive events.  
        this.startConnection();  
    }  

    private startConnection(): void {  
        this.connection.start().done((data: any) => {  
            console.log('Now connected ' + data.transport.name + ', connection ID = ' + data.id); 
            this.connectionEstablished.emit(true);  
            this.connectionExists = true;  
        }).fail((error: any) => {  
            console.log('Could not connect ' + error);  
            this.connectionEstablished.emit(false);  
        });  
    }  
    
    private registerOnServerEvents(): void {  
        this.proxy.on('changeStatus', (receivedData) => {  
            console.log('received in SignalRService: ' + JSON.stringify(receivedData));
            let data = {id: receivedData.Id, fileName: receivedData.FileName, fileStatus: receivedData.FileStatus};  
            this.dataReceived.emit(data);  
        });  
    }  
}