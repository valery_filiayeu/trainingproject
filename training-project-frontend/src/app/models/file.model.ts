import { UUID } from 'angular2-uuid';

export class FileModel {
    id: UUID;
    fileName: string;
    fileStatus: string;
}