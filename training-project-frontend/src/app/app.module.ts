import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { UploaderComponent } from './components/uploader/uploader.component';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';
import { ProcessComponent } from './components/process/process.component';
import { Routes, RouterModule } from '@angular/router';
import { StatusFileService } from './services/status-file.service';
import { SignalRModule } from 'ng2-signalr';
import { SignalRConfiguration } from 'ng2-signalr';
import { FileService } from './services/file.service';

const appRoutes: Routes =[
  { path: '', component: UploaderComponent},
  // { path: 'process', component: ProcessComponent},
  { path: '**', redirectTo: '/' }
];

export function createConfig(): SignalRConfiguration {
  const c = new SignalRConfiguration();
  c.hubName = 'Ng2SignalRHub';
  c.qs = { user: 'donald' };
  c.url = 'http://ng2-signalr-backend.azurewebsites.net/';
  c.logging = true;
  
  c.executeEventsInZone = true; // optional, default is true
  c.executeErrorsInZone = false; // optional, default is false
  c.executeStatusChangeInZone = true; // optional, default is true
  return c;
}

@NgModule({
  declarations: [
    AppComponent,
    UploaderComponent,
    FileSelectDirective,
    FileDropDirective,
    ProcessComponent
  ],
  imports: [
    BrowserModule,
    CommonModule, 
    FormsModule, 
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    SignalRModule.forRoot(createConfig)
  ],
  
  providers: [
    FileService,
    StatusFileService,
  ],
  
  bootstrap: [AppComponent]
})

export class AppModule { }
