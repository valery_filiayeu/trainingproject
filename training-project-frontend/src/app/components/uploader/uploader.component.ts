import { Component, OnInit, NgZone } from '@angular/core';
import { NgModule } from '@angular/core'
import { Http } from '@angular/http';
import { FileService } from '../../services/file.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { Router } from '@angular/router';
import { FileModel } from '../../models/file.model';
import { StatusFileService } from '../../services/status-file.service';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

const UPLOAD_URL = 'http://localhost:51018/api/upload/UploadFile'; 
const NO_PROCESSED_FILES_URL = 'http://localhost:51018/api/upload/GetNoProcessedFiles';
// const UPLOAD_URL = 'http://localhost:55736/api/upload/UploadFile/'; 

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.css'],
})
export class UploaderComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({url: UPLOAD_URL})

  files: FileModel[] = [];
  file: FileModel = new FileModel();
  form: FormGroup;
  idsArray: FormArray = new FormArray([]);
  done: boolean;

  private canSendMessage: Boolean;  

  constructor(private fileService: FileService, private fb: FormBuilder, private router: Router, private signalRService: StatusFileService, private ngZone: NgZone) {  }  

  ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log("Uploaded:", item, status, response);
      this.getNoProcessedFiles();
    };
    this.subscribeToEvents();  
    this.canSendMessage = this.signalRService.connectionExists;  
    this.getNoProcessedFiles();
  }
  
  private getNoProcessedFiles() {
    return this.fileService.getNoProcessedFiles(NO_PROCESSED_FILES_URL).subscribe((data) => { this.files = data; });
  } 

  private subscribeToEvents(): void {  
    // if connection exists it can call of method.  
    this.signalRService.connectionEstablished.subscribe(() => {  
        this.canSendMessage = true;  
    });  

    this.signalRService.dataReceived.subscribe((data: FileModel) => {    
      this.ngZone.run(() => {  
        this.changeStatus(data);           
      }); 

    });
  }  

  private changeStatus(receivedData: FileModel) {
    while (this.files.length > 0) {
      for (let file of this.files) {
        if (receivedData.id == file.id) {
          file.fileStatus = receivedData.fileStatus;
        }
      }
      break;
    }
  }
}