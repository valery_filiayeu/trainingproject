﻿using TrainingProject.Loggers;
using TrainingProjectBase.Directors;
using TrainingProjectBase.Directors.Implementation;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Managers;
using TrainingProjectBase.Managers.Implementation.Sql;
using TrainingProjectBase.Readers;
using TrainingProjectBase.Readers.Implementation;
using TrainingProjectBase.Repository;
using TrainingProjectBase.Repository.Implementation;
using TrainingProjectBase.Savers;
using TrainingProjectBase.Savers.Implementation;
using TrainingProjectBase.Transformers;
using TrainingProjectBase.Transformers.Implementation;
using TrainingProjectBase.Utils;
using TrainingProjectBase.Utils.Implementation;
using Unity;

namespace TrainingProject.DependencyInjection
{
    public static class DependencyInjectionMapper
    {
        public static void Map(IUnityContainer container)
        {
            container.RegisterInstance(container);
            MapDataAccess(container);
        }

        private static void MapDataAccess(IUnityContainer container)
        {
            container.RegisterType<IContextFactory, ContextFactory>();
            container.RegisterType(typeof(IRepository<,>), typeof(Repository<,>));
            container.RegisterType<IUnitOfWork, UnitOfWork>();

            container.RegisterType<ISongManager, SongManagerSql>();
            container.RegisterType<IGenreManager, GenreManagerSql>();
            container.RegisterType<IArtistManager, ArtistManagerSql>();

            container.RegisterType(typeof(IStreamReader<>), typeof(StreamReader<>));
            container.RegisterType(typeof(ISqlDataUtil<>), typeof(SqlDataUtil<>));
            container.RegisterType<IDataTransformer, DataTransformer>();
            container.RegisterType<IDataSaver, SongsDatabaseSaver>();

            container.RegisterType<IDataDirector, CsvDataDirector>();

            container.RegisterType<ILogger, ConsoleLogger>();
        }

    }
}
