﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using TrainingProject.DependencyInjection;
using TrainingProjectBase.Directors;
using Unity;

namespace TrainingProject
{
    sealed class Program
    {
        static void Main(string[] args)
        {
            var filePath = ConfigurationManager.AppSettings["pathToCsvFile"];
            var stopwatch = new Stopwatch();
            var container = MapContainer();
            var dataDirector = container.Resolve<IDataDirector>();
            
            Console.WriteLine("Start");

            stopwatch.Start();

            dataDirector.AddDatabase(ConvertFileToStream(filePath));

            stopwatch.Stop();
            Console.WriteLine("Finished in {0}", stopwatch.Elapsed);

            Console.ReadKey();
        }

        private static UnityContainer MapContainer()
        {
            var container = new UnityContainer();
            DependencyInjectionMapper.Map(container);
            return container;
        }

        private static Stream ConvertFileToStream(string filePath)
        {
            MemoryStream stream = null;
            using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                byte[] buf = new byte[fs.Length];
                fs.Read(buf, 0, Convert.ToInt32(fs.Length));
                stream = new MemoryStream(buf);
            }
            return stream;
        }
    }
}
