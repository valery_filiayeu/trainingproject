﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using TrainingProjectBase.Models.CsvModel;
using TrainingProjectBase.Readers.Implementation;

namespace TrainingProjectTest.Readers.Implementation
{
    [TestClass]
    public class CsvFileReaderTest
    {
        const string TestLyricsFile = "test_lyrics_file.csv";
        const string TestEmptyFile = "test_empty_file.csv";

        [TestMethod]
        public void ReadFile_ShouldRead_FromEmptyFile_Correctly()
        {
            var streamReader = new StreamReader<CsvSong>();
            var stream = ConvertFileToStream(TestEmptyFile);
            var list = streamReader.ReadStream(stream);
            Assert.AreEqual(0, list.Count);
        }

        [TestMethod]
        public void ReadFile_ShouldReturn_NotEmptyList_If_FileNotEmpty()
        {
            var streamReader = new StreamReader<CsvSong>();
            var stream = ConvertFileToStream(TestLyricsFile);
            var list = streamReader.ReadStream(stream);
            Assert.AreNotEqual(0, list.Count);
        }

        [TestMethod]
        public void ReadFile_ShouldReturn_ListWithCount1_If_FileHas1Record()
        {
            var streamReader = new StreamReader<CsvSong>();
            var stream = ConvertFileToStream(TestLyricsFile);
            var list = streamReader.ReadStream(stream);
            Assert.AreEqual(1, list.Count);
        }

        private Stream ConvertFileToStream(string filePath)
        {
            MemoryStream stream = null;
            using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                byte[] buf = new byte[fs.Length];
                fs.Read(buf, 0, Convert.ToInt32(fs.Length));
                stream = new MemoryStream(buf);
            }
            return stream;
        }

    }
}
