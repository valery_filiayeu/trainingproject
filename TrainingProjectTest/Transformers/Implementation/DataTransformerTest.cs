﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Models.CsvModel;
using TrainingProjectBase.Transformers.Implementation;

namespace TrainingProjectTest.Transformers.Implementation
{
    [TestClass]
    public class DataTransformerTest
    {

        IList<CsvSong> csvList = new List<CsvSong>()
        {
            new CsvSong()
            {
                Index = 1,
                Song = "People are strange",
                Year = 1967,
                Artist = "The Doors",
                Genre = "rock",
                Lyrics = "People are strange when you're a stranger..."
            },

            new CsvSong()
            {
                Index = 2,
                Song = "Come together",
                Year = 1969,
                Artist = "The Beatles",
                Genre = "rock",
                Lyrics = "Come together right now over me..."
            },

            new CsvSong()
            {
                Index = 3,
                Song = "The End",
                Year = 1967,
                Artist = "The Doors",
                Genre = "rock",
                Lyrics = "This is the end..."
            },

        };

        [TestMethod]
        public void SetAllLists_ShouldReturn_ArtistsListWithCount2_If_CsvList_Has2IdenticalArtists_Of3()
        {
            var mock = new Mock<ILogger>();
            DataTransformer transformer = new DataTransformer(mock.Object);
            var transformerResult = transformer.SetAllLists(csvList);
            var artistsList = transformerResult.ArtistsList;
            Assert.AreEqual(2, artistsList.Count);
        }

        [TestMethod]
        public void SetAllLists_ShouldReturn_GenresListWithCount1_If_CsvList_Has3IdenticalGenres_Of3()
        {
            var mock = new Mock<ILogger>();
            DataTransformer transformer = new DataTransformer(mock.Object);
            var transformerResult = transformer.SetAllLists(csvList);
            var genresList = transformerResult.GenresList;
            Assert.AreEqual(1, genresList.Count);
        }

        [TestMethod]
        public void SetAllLists_ShouldReturn_SongsListWithCount3_If_CsvListCount3()
        {
            var mock = new Mock<ILogger>();
            DataTransformer transformer = new DataTransformer(mock.Object);
            var transformerResult = transformer.SetAllLists(csvList);
            var songsList = transformerResult.SongsList;
            Assert.AreEqual(3, songsList.Count);
        }

    }
}
