﻿using System.Diagnostics;
using System.Web.Http;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Directors;
using System.Web;
using TrainingProjectBase.Utils;
using TrainingProjectBase.Dto;
using System.Collections.Generic;
using System;
using System.Linq;
using TrainingProjectBase.SignalR.Hubs;

namespace TrainingProjectWebApi.Controllers
{
    public class UploadController : ApiController
    {
        private IDataDirector _dataDirector;
        private ISqlFileUtil _sqlFileUtil;
        private ILogger _logger;
 
        public UploadController(IDataDirector dataDirector, ILogger logger, ISqlFileUtil sqlFileUtil)
        {
            _dataDirector = dataDirector;
            _logger = logger;
            _sqlFileUtil = sqlFileUtil;
        }

        [HttpGet]
        [Route("api/upload/GetNoProcessedFiles")]
        public IList<FileIdAndNameDto> GetNoProcessedFiles()
        {
            return _sqlFileUtil.SelectNoProcessedFiles();
        }

        [HttpPost]
        [Route("api/upload/UploadFile")]
        public Guid UploadFile()
        {
            _logger.Log("Start");
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            HttpPostedFile postedFile = null;
            var httpRequest = HttpContext.Current.Request;

            if (httpRequest.Files.Count > 0)
            {
                foreach (string file in httpRequest.Files)
                {
                    postedFile = httpRequest.Files[file];
                }
            }
            var id = _sqlFileUtil.InsertFile(postedFile);
            stopwatch.Stop();
            _logger.Log("Uploading file: " + stopwatch.Elapsed);


            return id;
        }

        [HttpPost]
        [Route("api/upload/Process")]
        public void Process(Guid[] idsArray)
        {
            List<Guid> idsList = idsArray.OfType<Guid>().ToList();
            foreach (var id in idsList)
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                var fileAllDataDto = _sqlFileUtil.SelectFile(id);
                
                var stream = fileAllDataDto.FileData;
                _dataDirector.AddDatabase(stream);
                _sqlFileUtil.UpdateFileToProcessed(id);
                stopwatch.Stop();
                _logger.Log("Process finished in: " + stopwatch.Elapsed);
            }
        }
    }
}
