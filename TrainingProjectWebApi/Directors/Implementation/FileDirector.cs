﻿using System.Data;
using System.Diagnostics;
using System.IO;
using System.Web;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Utils;

namespace TrainingProjectWebApi.Directors.Implementation
{
    public class FileDirector : IFileDirector
    {
        private ISqlFileUtil _fileUtil;
        private ILogger _logger;

        public FileDirector(ILogger logger, ISqlFileUtil fileUtil)
        {
            _fileUtil = fileUtil;
            _logger = logger;
        }

        public Stream GetUploadedFile(HttpPostedFile postedFile)
        {
            var id = _fileUtil.InsertFile(postedFile);
            var fileDto = _fileUtil.SelectFile(id);
            var stream = fileDto.FileData;
            return stream;
        }
    }
}