﻿using System.IO;
using System.Web;

namespace TrainingProjectWebApi.Directors
{
    public interface IFileDirector
    {
        Stream GetUploadedFile(HttpPostedFile postedFile);
    }
}
