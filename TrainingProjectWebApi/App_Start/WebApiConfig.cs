﻿using System.Web.Http;
using System.Web.Http.Cors;
using TrainingProjectBase.Directors;
using TrainingProjectBase.Directors.Implementation;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Managers;
using TrainingProjectBase.Managers.Implementation.Sql;
using TrainingProjectBase.Readers;
using TrainingProjectBase.Readers.Implementation;
using TrainingProjectBase.Repository;
using TrainingProjectBase.Repository.Implementation;
using TrainingProjectBase.Savers;
using TrainingProjectBase.Savers.Implementation;
using TrainingProjectBase.Transformers;
using TrainingProjectBase.Transformers.Implementation;
using TrainingProjectBase.Utils;
using TrainingProjectBase.Utils.Implementation;
using TrainingProjectWebApi.Directors;
using TrainingProjectWebApi.Directors.Implementation;
using TrainingProjectWebApi.Loggers;
using TrainingProjectWebApi.Unity;
using Unity;

namespace TrainingProjectWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            RoutesConfig(config);

            CorsConfig(config);

            UnityConfig(config);

            // Enable JSON Formatter
            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }

        private static void CorsConfig(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
        }

        private static void RoutesConfig(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static void UnityConfig(HttpConfiguration config)
        {
            var container = new UnityContainer();
            container.RegisterInstance(container);
            container.RegisterType<IContextFactory, ContextFactory>();
            container.RegisterType(typeof(IRepository<,>), typeof(Repository<,>));
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            //container.RegisterType<IUserManager, UserManager>();

            container.RegisterType<ISongManager, SongManagerSql>();
            container.RegisterType<IGenreManager, GenreManagerSql>();
            container.RegisterType<IArtistManager, ArtistManagerSql>();

            container.RegisterType(typeof(IStreamReader<>), typeof(StreamReader<>));
            container.RegisterType(typeof(ISqlDataUtil<>), typeof(SqlDataUtil<>));

            container.RegisterType<IDataTransformer, DataTransformer>();
            container.RegisterType<IDataSaver, SongsDatabaseSaver>();

            container.RegisterType<IDataDirector, CsvDataDirector>();

            container.RegisterType<ILogger, DebugLogger>();

            container.RegisterType<ISqlFileUtil, SqlFileUtil>();

            container.RegisterType<IFileDirector, FileDirector>();

            config.DependencyResolver = new UnityResolver(container);
        }
    }
}
