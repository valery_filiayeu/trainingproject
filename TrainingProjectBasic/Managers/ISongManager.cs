﻿using System.Collections.Generic;
using TrainingProjectBase.Models.Entities.Common;

namespace TrainingProjectBase.Managers
{
    public interface ISongManager
    {
        void AddAllSongs(IList<Song> songs);

        void RemoveAllSongs();
    }
}
