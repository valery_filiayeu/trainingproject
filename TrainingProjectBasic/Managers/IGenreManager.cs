﻿using System.Collections.Generic;
using TrainingProjectBase.Models.Entities.Common;

namespace TrainingProjectBase.Managers
{
    public interface IGenreManager
    {
        void AddAllGenres(IList<Genre> genres);

        void RemoveAllGenres();
    }
}
