﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Models.Entities.Common;
using TrainingProjectBase.Repository;

namespace TrainingProjectBase.Managers.Implementation.EF
{
    public class SongManagerEF : ISongManager
    {
        private IUnitOfWork _unitOfWork;
        private IRepository<Guid, Song> _repository;
        private ILogger _logger;

        public SongManagerEF(IUnitOfWork unitOfWork, ILogger logger)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.GetRepository<Guid, Song>();
        }

        public void AddAllSongs(IList<Song> songs)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            _repository.AddRange(songs);
            _unitOfWork.Commit();
            stopwatch.Stop();
            _logger.Log("Adding list of Songs to database: " + stopwatch.Elapsed);
        }

        public void RemoveAllSongs()
        {
            _repository.RemoveRange(GetAllSongs());
            _unitOfWork.Commit();
        }

        private IQueryable<Song> GetAllSongs()
        {
            return _repository.GetAllAsQueryable();
        }
    }
}
