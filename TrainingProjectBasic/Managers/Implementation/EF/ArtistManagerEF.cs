﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Models.Entities.Common;
using TrainingProjectBase.Repository;

namespace TrainingProjectBase.Managers.Implementation.EF
{
    public class ArtistManagerEF : IArtistManager
    {
        private IUnitOfWork _unitOfWork;
        private IRepository<Guid, Artist> _repository;
        private ILogger _logger;

        public ArtistManagerEF(IUnitOfWork unitOfWork, ILogger logger)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.GetRepository<Guid, Artist>();
        }


        public void AddAllArtists(IList<Artist> artists)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            _repository.AddRange(artists);
            _unitOfWork.Commit();
            stopwatch.Stop();
            _logger.Log("Adding list of Artists to database " + stopwatch.Elapsed);
        }

        private IQueryable<Artist> GetAllArtists()
        {
            return _repository.GetAllAsQueryable();
        }

        public void RemoveAllArtists()
        {
            _repository.RemoveRange(GetAllArtists());
            _unitOfWork.Commit();
        }
    }
}
