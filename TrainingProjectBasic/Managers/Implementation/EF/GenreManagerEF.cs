﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Models.Entities.Common;
using TrainingProjectBase.Repository;

namespace TrainingProjectBase.Managers.Implementation.EF
{
    public class GenreManagerEF : IGenreManager
    {
        private IUnitOfWork _unitOfWork;
        private IRepository<Guid, Genre> _repository;
        private ILogger _logger;

        public GenreManagerEF(IUnitOfWork unitOfWork, ILogger logger)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.GetRepository<Guid, Genre>();
        }


        public void AddAllGenres(IList<Genre> genres)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            _repository.AddRange(genres);
            _unitOfWork.Commit();
            stopwatch.Stop();
            _logger.Log("Adding list of Genres to database: " + stopwatch.Elapsed);
        }

        private IQueryable<Genre> GetAllGenres()
        {
            return _repository.GetAllAsQueryable();
        }

        public void RemoveAllGenres()
        {
            _repository.RemoveRange(GetAllGenres());
            _unitOfWork.Commit();
        }
    }
}
