﻿using System.Collections.Generic;
using System.Diagnostics;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Models.Entities.Common;
using TrainingProjectBase.Utils;

namespace TrainingProjectBase.Managers.Implementation.Sql
{
    public class GenreManagerSql : IGenreManager
    {
        const string TableName = "Genre";
        private ISqlDataUtil<Genre> _sqlUtil;
        private ILogger _logger;

        public GenreManagerSql(ISqlDataUtil<Genre> sqlUtil, ILogger logger)
        {
            _logger = logger;
            _sqlUtil = sqlUtil;
        }

        public void AddAllGenres(IList<Genre> genres)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            _sqlUtil.InsertAllData(genres, TableName);
            stopwatch.Stop();
            _logger.Log("Adding list of Genres to database: " + stopwatch.Elapsed);
        }

        public void RemoveAllGenres()
        {
            _sqlUtil.DeleteAllData(TableName);
        }
    }
}
