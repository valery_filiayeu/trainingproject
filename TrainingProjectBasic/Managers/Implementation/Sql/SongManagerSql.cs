﻿using System.Collections.Generic;
using System.Diagnostics;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Models.Entities.Common;
using TrainingProjectBase.Utils;

namespace TrainingProjectBase.Managers.Implementation.Sql
{
    public class SongManagerSql : ISongManager
    {
        const string TableName = "Song";

        private ISqlDataUtil<Song> _sqlUtil;
        private ILogger _logger;

        public SongManagerSql(ISqlDataUtil<Song> sqlUtil, ILogger logger)
        {
            _logger = logger;
            _sqlUtil = sqlUtil;
        }

        public void AddAllSongs(IList<Song> songs)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            _sqlUtil.InsertAllData(songs, TableName);
            stopwatch.Stop();
            _logger.Log("Adding list of Songs to database: " + stopwatch.Elapsed);

        }

        public void RemoveAllSongs()
        {
            _sqlUtil.DeleteAllData(TableName);
        }
    }
}
