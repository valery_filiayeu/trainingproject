﻿using System.Collections.Generic;
using System.Diagnostics;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Models.Entities.Common;
using TrainingProjectBase.Utils;

namespace TrainingProjectBase.Managers.Implementation.Sql
{
    public class ArtistManagerSql : IArtistManager
    {
        const string TableName = "Artist";

        private ISqlDataUtil<Artist> _sqlUtil;
        private ILogger _logger;

        public ArtistManagerSql(ISqlDataUtil<Artist> sqlUtil, ILogger logger)
        {
            _logger = logger;
            _sqlUtil = sqlUtil;
        }

        public void AddAllArtists(IList<Artist> artists)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            _sqlUtil.InsertAllData(artists, TableName);
            stopwatch.Stop();
            _logger.Log("Adding list of Artists to database: " + stopwatch.Elapsed);
        }

        public void RemoveAllArtists()
        {
            _sqlUtil.DeleteAllData(TableName);
        }
    }
}
