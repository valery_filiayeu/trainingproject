﻿using System.Collections.Generic;
using TrainingProjectBase.Models.Entities.Common;

namespace TrainingProjectBase.Managers
{
    public interface IArtistManager
    {
        void AddAllArtists(IList<Artist> artists);

        void RemoveAllArtists();
    }
}
