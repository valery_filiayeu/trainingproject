﻿using System.Collections.Generic;
using System.Linq;
using TrainingProjectBase.Models;

namespace TrainingProjectBase.Repository
{
    public interface IRepository<TId, TEntity> where TEntity : class, IIdentityEntity<TId> where TId : struct
    {
        //TEntity Add(TEntity entity);
        IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities);

        //void Remove(TEntity entity);
        void RemoveRange(IQueryable<TEntity> entities);

        IQueryable<TEntity> GetAllAsQueryable();
    }
}
