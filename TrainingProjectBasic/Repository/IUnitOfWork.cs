﻿using System;
using TrainingProjectBase.Models;

namespace TrainingProjectBase.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TId, TEntity> GetRepository<TId, TEntity>() where TEntity : class, IIdentityEntity<TId> where TId : struct;

        void Commit(Guid? userAccountId = null);
    }
}
