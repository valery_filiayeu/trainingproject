﻿using TrainingProjectBase.Context;

namespace TrainingProjectBase.Repository
{
    public interface IContextFactory
    {
        IDatabaseContext CreateDatabaseContext();
    }
}
