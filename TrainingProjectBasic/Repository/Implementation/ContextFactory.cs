﻿using TrainingProjectBase.Context;
using TrainingProjectBase.Context.Implementation;

namespace TrainingProjectBase.Repository.Implementation
{
    public class ContextFactory : IContextFactory
    {
        public IDatabaseContext CreateDatabaseContext()
        {
            return new DatabaseContext();
        }
    }
}
