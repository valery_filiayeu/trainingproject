﻿using System.Collections.Generic;
using System.Linq;
using TrainingProjectBase.Context;
using TrainingProjectBase.Models;

namespace TrainingProjectBase.Repository.Implementation
{
    public class Repository<TId, TEntity> : IRepository<TId, TEntity> where TEntity : class, IIdentityEntity<TId> where TId : struct
    {
        private readonly IDatabaseContext _context;

        public Repository(IDatabaseContext context)
        {
            _context = context;
        }

        public IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities)
        {
            return _context.Set<TEntity>().AddRange(entities);
        }

        public void RemoveRange(IQueryable<TEntity> entities)
        {
            _context.Set<TEntity>().RemoveRange(entities);
        }

        public IQueryable<TEntity> GetAllAsQueryable()
        {
            return _context.Set<TEntity>();
        }
    }
}
