﻿using System.Collections.Generic;
using System.IO;

namespace TrainingProjectBase.Readers
{
    public interface IStreamReader<T>
    {
        IList<T> ReadStream(Stream stream);
    }
}
