﻿using CsvHelper;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TrainingProjectBase.Readers.Implementation
{
    public class StreamReader<T> : IStreamReader<T>
    {
        public IList<T> ReadStream(Stream stream)
        {
            IList<T> list = null;
            using (var sr = new StreamReader(stream))
            {
                var reader = new CsvReader(sr);
                var enumerable = reader.GetRecords<T>();
                list = enumerable.ToList();
            }
            return list;
        }
    }
}
