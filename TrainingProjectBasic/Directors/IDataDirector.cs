﻿using System.IO;

namespace TrainingProjectBase.Directors
{
    public interface IDataDirector
    {
        void AddDatabase(Stream stream);
    }
}
