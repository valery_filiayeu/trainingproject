﻿using System.Configuration;
using System.Collections.Generic;
using System.Diagnostics;
using TrainingProjectBase.Models.CsvModel;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Transformers;
using TrainingProjectBase.Readers;
using TrainingProjectBase.Savers;
using TrainingProjectBase.Transformers.Common;
using System.IO;

namespace TrainingProjectBase.Directors.Implementation
{
    public class CsvDataDirector : IDataDirector
    {
        private IStreamReader<CsvSong> _fileUtil;
        private IDataTransformer _transformer;
        private IDataSaver _saver;
        private ILogger _logger;

        public CsvDataDirector(IStreamReader<CsvSong> fileUtil, IDataTransformer transformer, IDataSaver saver, ILogger logger)
        {
            _logger = logger;
            _fileUtil = fileUtil;
            _transformer = transformer;
            _saver = saver;
        }

        private IList<CsvSong> ReadFromStream(Stream stream)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var csvList = _fileUtil.ReadStream(stream);
            stopwatch.Stop();
            _logger.Log("Reading from file: " + stopwatch.Elapsed);
            return csvList;
        }

        private TransformerResult TransformData(Stream stream)
        {
            var csvList = ReadFromStream(stream);
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var transformerResult = _transformer.SetAllLists(csvList);
            stopwatch.Stop();
            _logger.Log("Transform of data: " + stopwatch.Elapsed);
            return transformerResult;
        }

        public void AddDatabase(Stream stream)
        {
            var transformerResult = TransformData(stream);
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            _saver.AddAllData(transformerResult);
            stopwatch.Stop();
            _logger.Log("Writing to Database: " + stopwatch.Elapsed);
        }
    }
}
