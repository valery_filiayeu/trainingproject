﻿namespace TrainingProjectBase.Models.CsvModel
{
    public class CsvSong
    {
        public long Index { get; set; }
        public string Song { get; set; }
        public long Year { get; set; }
        public string Artist { get; set; }
        public string Genre { get; set; }
        public string Lyrics { get; set; }
    }
}
