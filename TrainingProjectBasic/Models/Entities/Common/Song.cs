﻿using System;
using TrainingProjectBase.Models.Entities.Base;

namespace TrainingProjectBase.Models.Entities.Common
{
    public class Song : DataEntity
    {
        public string Name { get; set; }
        public long Year { get; set; }
        public string Lyrics { get; set; }
        public Guid GenreId { get; set; }
        public Genre Genre { get; set; }
        public Guid ArtistId { get; set; }
        public Artist Artist { get; set; }
    }
}
