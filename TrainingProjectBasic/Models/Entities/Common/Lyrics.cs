﻿using System;
using TrainingProjectBase.Models.Entities.Base;

namespace TrainingProjectBase.Models.Entities.Common
{
    public class Lyrics : DataEntity
    {
        public string Text { get; set; }
        public Guid SongId { get; set; }
        public Song Song { get; set; }
    }
}
