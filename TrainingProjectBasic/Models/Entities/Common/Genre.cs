﻿using TrainingProjectBase.Models.Entities.Base;

namespace TrainingProjectBase.Models.Entities.Common
{
    public class Genre : DataEntity
    {
        public string Name { get; set; }
    }
}
