﻿using TrainingProjectBase.Models.Entities.Base;

namespace TrainingProjectBase.Models.Entities.Common
{
    public class Artist : DataEntity
    {
        public string Name { get; set; }
    }
}
