﻿using System;

namespace TrainingProjectBase.Dto
{
    public class FileIdAndNameDto
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }

        public static FileIdAndNameDto MapFromFileTable(Guid id, string fileName)
        {
            return new FileIdAndNameDto
            {
                Id = id,
                FileName = fileName,
            };
        }
    }
}