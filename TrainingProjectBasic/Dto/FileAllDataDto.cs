﻿using System;
using System.IO;

namespace TrainingProjectBase.Dto
{
    public class FileAllDataDto
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public Stream FileData { get; set; }

        public static FileAllDataDto MapFromFileTable(Guid id, string fileName, byte[] fileData)
        {
            return new FileAllDataDto
            {
                Id = id,
                FileName = fileName,
                FileData = ConvertByteToStream(fileData)
            };
        }

        private static Stream ConvertByteToStream(byte[] fileData)
        {
            var stream = new MemoryStream(fileData);
            return stream;
        }
    }
}