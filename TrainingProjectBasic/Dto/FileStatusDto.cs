﻿using System;

namespace TrainingProjectBase.Dto
{
    public class FileStatusDto
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public string FileStatus { get; set; }

        public static FileStatusDto MapFileStatus(Guid id, string fileName, string fileStatus)
        {
            return new FileStatusDto
            {
                Id = id,
                FileName = fileName,
                FileStatus = fileStatus,
            };
        }
    }
}
