﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace TrainingProjectBase.Context
{
    public interface IDatabaseContext : IDisposable
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbChangeTracker ChangeTracker { get; }
        DbEntityEntry Entry(object entity);
        int SaveChanges();
    }
}
