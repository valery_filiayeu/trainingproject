﻿using System.Data.Entity;

namespace TrainingProjectBase.Context.Implementation
{
    public class DatabaseContext : DbContext, IDatabaseContext
    {
        public DatabaseContext() : base("Database") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            Database.SetInitializer<DatabaseContext>(null);

            modelBuilder.Configurations.AddFromAssembly(typeof(DatabaseContext).Assembly);
        }
    }
}
