﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Models.CsvModel;
using TrainingProjectBase.Models.Entities.Common;
using TrainingProjectBase.Transformers.Common;

namespace TrainingProjectBase.Transformers.Implementation
{
    public class DataTransformer : IDataTransformer
    {
        private ILogger _logger;

        public DataTransformer(ILogger logger)
        {
            _logger = logger;
        }

        public TransformerResult SetAllLists(IList<CsvSong> csvList)
        {
            var transformerResult = new TransformerResult()
            {
                ArtistsList =  SetArtistsList(csvList),
                GenresList = SetGenresList(csvList),
                SongsList = SetSongsList(csvList)
            };
            return transformerResult;
        }

        private IList<Artist> SetArtistsList(IList<CsvSong> csvList)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var artistsList = csvList
                .Select(artistName => artistName.Artist)
                .Distinct()
                .Select(artist => new Artist
                {
                    Id = Guid.NewGuid(),
                    Name = artist
                }).ToList();
            stopwatch.Stop();
            _logger.Log("Set Artists List: " + stopwatch.Elapsed);
            return artistsList;
        }

        private IList<Genre> SetGenresList(IList<CsvSong> csvList)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var genresList = csvList
                .Select(genreName => genreName.Genre)
                .Distinct()
                .Select(genre => new Genre
                {
                    Id = Guid.NewGuid(),
                    Name = genre
                }).ToList();
            stopwatch.Stop();
            _logger.Log("Set Genres List: " + stopwatch.Elapsed);
            return genresList;
        }

        private IList<Song> SetSongsList(IList<CsvSong> csvList)
        {
            var artistsList = SetArtistsList(csvList);
            var genresList = SetGenresList(csvList);

            var artistsDictionary = artistsList.ToDictionary(key => key.Name, value => value.Id);
            var genresDictionary = genresList.ToDictionary(key => key.Name, value => value.Id);

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var songsList = csvList
                .Select(song => new Song
                {
                    Id = Guid.NewGuid(),
                    Name = song.Song,
                    Year = song.Year,
                    Lyrics = song.Lyrics,
                    ArtistId = artistsDictionary[song.Artist],
                    GenreId = genresDictionary[song.Genre],

                }).Take(10).ToList();
            stopwatch.Stop();
            _logger.Log("Set Songs List: " + stopwatch.Elapsed);
            return songsList;
        }
    }
}
