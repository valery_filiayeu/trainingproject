﻿using System.Collections.Generic;
using TrainingProjectBase.Models.CsvModel;
using TrainingProjectBase.Transformers.Common;

namespace TrainingProjectBase.Transformers
{
    public interface IDataTransformer
    {
        TransformerResult SetAllLists(IList<CsvSong> csvList);
    }
}
