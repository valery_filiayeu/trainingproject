﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainingProjectBase.Models.Entities.Common;

namespace TrainingProjectBase.Transformers.Common
{
    public class TransformerResult
    {
        public IList<Artist> ArtistsList { get; set; }
        public IList<Genre> GenresList { get; set; }
        public IList<Song> SongsList { get; set; }
    }
}
