﻿using System.Diagnostics;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Managers;
using TrainingProjectBase.Transformers.Common;

namespace TrainingProjectBase.Savers.Implementation
{
    public class SongsDatabaseSaver : IDataSaver
    {
        private ISongManager _songManager;
        private IGenreManager _genreManager;
        private IArtistManager _artistManager;
        private ILogger _logger;

        public SongsDatabaseSaver(ISongManager songManager, IGenreManager genreManager, IArtistManager artistManager, ILogger logger)
        {
            _songManager = songManager;
            _artistManager = artistManager;
            _genreManager = genreManager;
            _logger = logger;
        }

        public void AddAllData(TransformerResult transformerResult)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            RemoveAllData();
            stopwatch.Stop();
            _logger.Log("Cleaning Database: " + stopwatch.Elapsed);

            _artistManager.AddAllArtists(transformerResult.ArtistsList);
            _genreManager.AddAllGenres(transformerResult.GenresList);
            _songManager.AddAllSongs(transformerResult.SongsList);
        }

        private void RemoveAllData()
        {
            _genreManager.RemoveAllGenres();
            _artistManager.RemoveAllArtists();
        }
    }
}
