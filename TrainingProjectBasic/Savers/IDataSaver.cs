﻿using TrainingProjectBase.Transformers.Common;

namespace TrainingProjectBase.Savers
{
    public interface IDataSaver
    {
        void AddAllData(TransformerResult transformerResult);
    }
}
