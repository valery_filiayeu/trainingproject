﻿namespace TrainingProjectBase.Loggers
{
    public interface ILogger
    {
        void Log(string message);
    }
}
