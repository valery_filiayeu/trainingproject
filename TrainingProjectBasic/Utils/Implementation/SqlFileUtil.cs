﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using TrainingProjectBase.Dto;
using TrainingProjectBase.Loggers;

namespace TrainingProjectBase.Utils.Implementation
{
    public class SqlFileUtil : ISqlFileUtil
    {
        const string TableName = "[TrainingProject].[dbo].[Files]";

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["Database"].ConnectionString;            
        }

        private static void OpenConnection(SqlConnection connection)
        {
            connection.ConnectionString = GetConnectionString();
            try
            {
                connection.Open();
                _logger.Log("Open connection");
            }
            catch (Exception e)
            {
                _logger.Log(e.Message);
            }
        }

        private static ILogger _logger;

        public SqlFileUtil(ILogger logger)
        {
            _logger = logger;
        }

        public Guid InsertFile(HttpPostedFile postedFile)
        {
            Guid newId = new Guid();

            int size = postedFile.ContentLength;
            var fileName = postedFile.FileName;

            byte[] fileData = new byte[size];
            postedFile.InputStream.Read(fileData, 0, size);

            using (var connection = new SqlConnection())
            {
                OpenConnection(connection);
                _logger.Log("Connection opened");
                var command = connection.CreateCommand();
                var transaction = connection.BeginTransaction();
                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = "INSERT INTO " + TableName + " ([FileName],[FileData]) OUTPUT INSERTED.Id VALUES(@FileName, @FileData)";
                    SqlParameter par = new SqlParameter();

                    command.Parameters.Add("@FileName", SqlDbType.NVarChar, 255);
                    command.Parameters.Add("@FileData", SqlDbType.VarBinary);
                    command.Parameters["@FileName"].Value = fileName;
                    command.Parameters["@FileData"].Value = fileData;

                    newId = (Guid)command.ExecuteScalar();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                
            }
            return newId;
        }

        public FileAllDataDto SelectFile(Guid id)
        {
            string fileName = null;
            byte[] fileData = null;

            using (var connection = new SqlConnection())
            {
                OpenConnection(connection);
                var command = connection.CreateCommand();
                var transaction = connection.BeginTransaction();
                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = "SELECT FileName, FileData FROM " + TableName + " WHERE Id = @Id";
                    command.Parameters.Add("@Id", SqlDbType.UniqueIdentifier);
                    command.Parameters["@Id"].Value = id;

                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        fileName = reader["FileName"] as string;
                        fileData = reader["FileData"] as byte[];
                    }
                    reader.Close();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
            return FileAllDataDto.MapFromFileTable(id, fileName, fileData);
        }

        private IList<FileIdAndNameDto> SelectNoProcessed(string request)
        {
            IList<FileIdAndNameDto> filesList = new List<FileIdAndNameDto>();
            Guid id = new Guid();
            string fileName = null;
            using (var connection = new SqlConnection())
            {
                OpenConnection(connection);
                var command = connection.CreateCommand();
                var transaction = connection.BeginTransaction();
                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = request;

                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        id = (Guid)reader["Id"];
                        fileName = reader["FileName"] as string;
                        filesList.Add(FileIdAndNameDto.MapFromFileTable(id, fileName));
                    }
                    reader.Close();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    _logger.Log(e.Message);
                    transaction.Rollback();
                }
                return filesList;
            }
        }

        public IList<FileIdAndNameDto> SelectNoProcessedFiles()
        {
            var request = "SELECT Id, FileName FROM " + TableName + " WHERE IsProcessed = 0";
            return SelectNoProcessed(request);
        }

        public IList<FileIdAndNameDto> SelectNoProcessedFile()
        {
            var request = "SELECT TOP 1 Id, FileName FROM " + TableName + " WHERE IsProcessed = 0";
            return SelectNoProcessed(request);
        }

        public void UpdateFileToProcessed(Guid id)
        {
            using (var connection = new SqlConnection())
            {
                OpenConnection(connection);
                var command = connection.CreateCommand();
                var transaction = connection.BeginTransaction();
                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = "UPDATE " + TableName + " SET IsProcessed = 1 WHERE Id = @Id";
                    command.Parameters.Add("@Id", SqlDbType.UniqueIdentifier);
                    command.Parameters["@Id"].Value = id;
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception )
                {
                    transaction.Rollback();
                }
            }
        }
    }
}