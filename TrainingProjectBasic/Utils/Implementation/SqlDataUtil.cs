﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace TrainingProjectBase.Utils.Implementation
{
    public class SqlDataUtil<T> : ISqlDataUtil<T>
    {
        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["Database"].ConnectionString;
        }

        private static void OpenConnection(SqlConnection connection)
        {
            connection.ConnectionString = GetConnectionString();
            connection.Open();
        }

        public void DeleteAllData(string tableName)
        {
            using (var connection = new SqlConnection())
            {
                OpenConnection(connection);
                var command = connection.CreateCommand();
                var transaction = connection.BeginTransaction();
                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = "DELETE FROM [" + tableName + "]";
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                connection.Close();
            }
        }

        public void InsertAllData(IList<T> outputList, string tableName)
        {
            var dt = new DataTable();
            dt = ConvertToDataTable(outputList);
            using (var connection = new SqlConnection())
            {
                OpenConnection(connection);
                var command = connection.CreateCommand();
                var transaction = connection.BeginTransaction();
                command.Connection = connection;
                command.Transaction = transaction;

                using (var bulkcopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.KeepIdentity, transaction))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    try
                    {
                        bulkcopy.DestinationTableName = tableName;
                        ColumnMappings(bulkcopy, command, tableName);               
                        bulkcopy.WriteToServer(dt);
                        transaction.Commit();
                        connection.Close();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                }
            }
        }

        private DataTable ConvertToDataTable(IList<T> outputList)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var dt = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                dt.Columns.Add(prop.Name, prop.PropertyType);
            }
            foreach (T item in outputList)
            {
                DataRow row = dt.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item);
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        private IList<string> GetColumnsName(SqlCommand command, string tableName)
        {
            var columnsNameList = new List<string>();
            command.CommandText = "SELECT * FROM [" + tableName + "]";                   
            using (var reader = command.ExecuteReader(CommandBehavior.KeyInfo))
            {
                var schemaTable = reader.GetSchemaTable();
                foreach (DataRow field in schemaTable.Rows)
                {
                    var columnName = field["BaseColumnName"].ToString();
                    columnsNameList.Add(columnName);
                }
            }
            return columnsNameList;
        }

        private void ColumnMappings(SqlBulkCopy bulkCopy, SqlCommand command, string tableName)
        {
            var columnsNameList = GetColumnsName(command, tableName);
            foreach (var columnName in columnsNameList)
            {
                bulkCopy.ColumnMappings.Add(columnName, columnName);
            }
        }
    }
}
