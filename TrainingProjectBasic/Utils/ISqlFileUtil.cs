﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TrainingProjectBase.Dto;

namespace TrainingProjectBase.Utils
{
    public interface ISqlFileUtil
    {
        Guid InsertFile(HttpPostedFile postedFile);

        FileAllDataDto SelectFile(Guid id);

        IList<FileIdAndNameDto> SelectNoProcessedFiles();

        IList<FileIdAndNameDto> SelectNoProcessedFile();

        void UpdateFileToProcessed(Guid id);
    }
}
