﻿using System.Collections.Generic;
using TrainingProjectBase.Models;

namespace TrainingProjectBase.Utils
{
    public interface ISqlDataUtil<T>
    {
        void DeleteAllData(string tableName);
        void InsertAllData(IList<T> outputList, string tableName);
    }
}
