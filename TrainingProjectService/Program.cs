﻿using System;
using System.ServiceProcess;
using TrainingProjectService.DependencyInjection;
using Unity;

namespace TrainingProjectService
{
    static class Program
    {

        private static UnityContainer MapContainer()
        {
            var container = new UnityContainer();
            DependencyInjectionMapper.Map(container);
            return container;
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            var container = MapContainer();

            ProcessFileService service = new ProcessFileService(container);

            if (Environment.UserInteractive)
            {
                service.RunAsConsole(args);
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] { service };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
     
