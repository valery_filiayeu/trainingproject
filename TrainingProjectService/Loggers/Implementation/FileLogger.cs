﻿using System;
using System.IO;
using TrainingProjectBase.Loggers;

namespace TrainingProjectService.Loggers
{
    public class FileLogger : ILogger
    {
        public void Log(string message)
        {
            using (var sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.log", true))
            {
                sw.WriteLine(DateTime.Now.ToString() + ": " + message);
            }
        }
    }
}
