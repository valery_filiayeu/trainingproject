﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using TrainingProjectBase.Directors;
using TrainingProjectBase.Dto;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.SignalR.Hubs;
using TrainingProjectBase.Utils;

namespace TrainingProjectService.Directors.Implementation
{
    public class ProcessFileDirector : IProcessFileDirector
    {
        private static ISqlFileUtil _sqlFileUtil;
        private static IDataDirector _dataDirector;
        private static ILogger _logger;

        public ProcessFileDirector(ISqlFileUtil sqlFileUtil, IDataDirector dataDirector, ILogger logger)
        {
            _sqlFileUtil = sqlFileUtil;
            _dataDirector = dataDirector;
            _logger = logger;
        }

        public void ProcessFile()
        {            
            IList<FileIdAndNameDto> filesList = _sqlFileUtil.SelectNoProcessedFile();
            if (filesList.Count != 0)
            {
                foreach (var file in filesList)
                {
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();
                    SendStatus(FileStatusDto.MapFileStatus(file.Id, file.FileName, "in progress"));
                    var fileAllDataDto = _sqlFileUtil.SelectFile(file.Id);
                    var stream = fileAllDataDto.FileData;
                    _dataDirector.AddDatabase(stream);
                    _sqlFileUtil.UpdateFileToProcessed(file.Id);
                    SendStatus(FileStatusDto.MapFileStatus(file.Id, file.FileName, "processed"));
                    stopwatch.Stop();
                    _logger.Log("Process finished in: " + stopwatch.Elapsed);
                    Console.WriteLine("Process finished in: " + stopwatch.Elapsed);   
                }   
            }
        }

        private void SendStatus(FileStatusDto fileStatusDto)
        {
            var hubContext = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<StatusHub>();
            hubContext.Clients.All.changeStatus(fileStatusDto);
        }
    }
}
