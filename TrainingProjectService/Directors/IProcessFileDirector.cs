﻿namespace TrainingProjectService.Directors
{
    public interface IProcessFileDirector
    {
        void ProcessFile();
    }
}
