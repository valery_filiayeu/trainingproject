﻿using System;
using System.ServiceProcess;
using Unity;
using TrainingProjectService.Directors;
using System.Timers;
using TrainingProjectBase.Loggers;
using System.Configuration;
using Microsoft.Owin.Hosting;
using TrainingProjectBase.SignalR;

namespace TrainingProjectService
{
    public partial class ProcessFileService : ServiceBase
    {
        private static UnityContainer _container;
        private static ILogger _logger;

        public void RunAsConsole(string[] args)
        {
            OnStart(args);
            Console.WriteLine("Press any key to exit...");
            Console.ReadLine();
            OnStop();
        }

        private Timer timer = null; 

        public ProcessFileService(UnityContainer container)
        {
            _container = container;
            InitializeComponent();

            this.CanStop = true;
            this.AutoLog = true;
        }

        protected override void OnStart(string[] args)
        {
            string url = @"http://localhost:8080";
            //using (WebApp.Start<Startup>(url))
            //{
            //    Console.WriteLine(string.Format("Server running at {0}", url));
            //    Console.ReadLine();
            //}

            try
            {
                WebApp.Start<Startup>(url);
                Console.WriteLine(string.Format("Server running at {0}", url));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            var processFileDirector = _container.Resolve<IProcessFileDirector>();
            _logger = _container.Resolve<ILogger>();
            _logger.Log("Service started");
           
            this.timer = new Timer();

            var interval = ConfigurationManager.AppSettings["Interval"];
            this.timer.Interval = double.Parse(interval);

            this.timer.Elapsed += new ElapsedEventHandler(ProcessFile_Elapsed);
            this.timer.Enabled = true;
        }

        private void ProcessFile_Elapsed(object obj, ElapsedEventArgs e)
        {
            var processFileDirector = _container.Resolve<IProcessFileDirector>();
            processFileDirector.ProcessFile();
        }

        protected override void OnStop()
        {
            timer.Enabled = false;
            _logger.Log("Service stopped");
        }
    }
}
