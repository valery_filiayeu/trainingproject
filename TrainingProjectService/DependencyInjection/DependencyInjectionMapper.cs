﻿using TrainingProjectBase.Directors;
using TrainingProjectBase.Directors.Implementation;
using TrainingProjectBase.Loggers;
using TrainingProjectBase.Managers;
using TrainingProjectBase.Managers.Implementation.Sql;
using TrainingProjectBase.Readers;
using TrainingProjectBase.Readers.Implementation;
using TrainingProjectBase.Savers;
using TrainingProjectBase.Savers.Implementation;
using TrainingProjectBase.Transformers;
using TrainingProjectBase.Transformers.Implementation;
using TrainingProjectBase.Utils;
using TrainingProjectBase.Utils.Implementation;
using TrainingProjectService.Directors;
using TrainingProjectService.Directors.Implementation;
using TrainingProjectService.Loggers;
using Unity;

namespace TrainingProjectService.DependencyInjection
{
    public static class DependencyInjectionMapper
    {
        public static void Map(IUnityContainer container)
        {
            container.RegisterInstance(container);
            MapDataAccess(container);
        }

        private static void MapDataAccess(IUnityContainer container)
        {
            container.RegisterType<ISongManager, SongManagerSql>();
            container.RegisterType<IGenreManager, GenreManagerSql>();
            container.RegisterType<IArtistManager, ArtistManagerSql>();

            container.RegisterType(typeof(IStreamReader<>), typeof(StreamReader<>));
            container.RegisterType(typeof(ISqlDataUtil<>), typeof(SqlDataUtil<>));
            container.RegisterType<IDataTransformer, DataTransformer>();
            container.RegisterType<IDataSaver, SongsDatabaseSaver>();

            container.RegisterType<IDataDirector, CsvDataDirector>();
            container.RegisterType<ISqlFileUtil, SqlFileUtil>();
            container.RegisterType<ILogger, FileLogger>();

            container.RegisterType<IProcessFileDirector, ProcessFileDirector>();
        }
    }
}
